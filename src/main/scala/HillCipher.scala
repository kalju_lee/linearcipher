import CipherObject._

object HillCipher extends App {

	val helpDialog = "decode [thing to decode]\nencode [thing to encode]\nsetkey [16 digit key]\nquit\n\nkey is: \n" + CipherObject.matKey.toString + "\nrecovery key is: \n" + CipherObject.matRecoveryKey.toString + "\n"

	def getInput(): String = {
		val input = readLine()
		if (input.length == 0) {
			getInput
		}
		val parts = input.split(" ")
		if (parts.length == 2) {
			parts(0) match {
				case "encode" => {
					println("encoding " + parts(1))
					println(CipherObject.matProcess(parts(1), CipherObject.matKey))
				}
				case "decode" => {
					println("decoding " + parts(1))
					println(CipherObject.matProcess(parts(1), CipherObject.matRecoveryKey))
				}
				case _ => println(helpDialog)
			}
		}
		else if (parts.length == 1) {
			parts(0) match {
				case "quit" => System.exit(0)
				case "help" => println(helpDialog)
				case _ => println(helpDialog)
			}
		}
		else if (parts.length == 17 && parts(0) == "setkey") {println("setting key\nWARNING: no checks are in place to insure an effective key")}
		else {println("your intentions are a mystery- type help for help")}
		getInput()
	}

	println("\nrunning...\n")
	println("welcome to hill ciphering!\ntype 'help' to see your options")
	getInput
}