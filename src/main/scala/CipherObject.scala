
import scala.math.BigInt.int2bigInt
import scala.BigInt
import Matrix._

// this started as being incidental to the homework problem, but 
// kinda took on a life of its own
object CipherObject {

  // setting up the map of characters to numbers
  // this can probably be done cleaner with a val
  var letterToNumber: Map[Char, Int] = (for (i <- 1 to 26) yield ((i + 96).asInstanceOf[Char] -> i)).toMap
  letterToNumber += ('.' -> 27)
  letterToNumber += ('?' -> 28)
  letterToNumber += (' ' -> 0)

  // map of numbers to characters
  var numberToLetter = letterToNumber.map(_.swap)

  // cipher matrix key
  var matKey = new Matrix(
    List(
      List(4, 17, 5, 1),
      List(15, 24, 12, 11),
      List(28, 2, 7, 13),
      List(3, 6, 21, 0)))

  // recovery key for the above encoding matrix
  // created by a call to a Matrix method
  var matRecoveryKey = createMatRecoveryKey(matKey)

  // the coded message for 1d
  val s = "voxosoy kqeqlsqi?.k?pils?qkd.avcen.?vbaksok tdbk e.ikimdscsqnfk?l?okeqikobetlukzs.ock ?oci.klsqi?.kdsvbi.ok?qukp?osdkd.avc?q?laosofk.lxosf?"

  def setMatKey(newKey: Array[Int]) = {
    matKey = new Matrix(
      List(
        List(newKey(1), newKey(2), newKey(3), newKey(4)),
        List(newKey(5), newKey(6), newKey(7), newKey(8)),
        List(newKey(9), newKey(10), newKey(11), newKey(12)),
        List(newKey(13), newKey(14), newKey(15), newKey(17))))
    matRecoveryKey = createMatRecoveryKey(matKey)
  }

  //determines the coded character
  def encode(x: Int, key: Key) = (x * key.a + key.b) % 29

  // finds a message character from an encoded one
  def decode(y: Int, key: Key) = ((BigInt(key.a).modInverse(29) * (posMod(y - key.b))) % 29).toInt

  // insures a positive number is used with with the reverse modulo above 
  def posMod(y: Int): Int = if (y < 0) posMod(y + 29) else y

  // takes two List[Int], returns the dot product
  def dot(a: List[Int], b: List[Int]) = {
    if (a.length != b.length)
      throw (new Exception("bad dot product values"))
    else
      (for (i <- 0 until a.length) yield a(i) * b(i)).foldLeft(0)(_ + _)
  }

  // takes two matrices, returns the modulo product.  good matrix dimensions
  // are enforced by the dot() method
  def matModMult(a: Matrix, b: Matrix): Matrix = {
    val x = (for (i <- 0 until b.byCol.length; j <- 0 until a.byRow.length)
      yield modDot(a.byRow(j), b.byCol(i))).toList.grouped(a.byRow(1).length).toList
    new Matrix(x.toList)
  }

  // matrix multiplication without the modulo.  i plan on combining
  // the matrix multiplication methods and having it take a function
  //as an argument, as well as moving them over to the Matrix class,
  // but that may have to wait till after the semester
  def matMult(a: Matrix, b: Matrix): Matrix = {
    val x = (for (i <- 0 until b.byCol.length; j <- 0 until a.byRow.length)
      yield dot(a.byRow(j), b.byCol(i))).toList.grouped(a.byRow(1).length).toList
    new Matrix(x.toList)
  }

  // returns the modulo of a dot product
  // this method is the product of laziness
  def modDot(a: List[Int], b: List[Int]) = {
    dot(a, b) % 29
  }

  // takes a string, organizes it into a matrix with 4 rows.
  // the string is padded out with spaces to fit the matrix
  def createMatMessage(message: String): Matrix = {
    val backwards = new Matrix((for (thing <- padMessage(message).toLowerCase.toList) yield letterToNumber(thing)).grouped(matKey.byRow.length).toList)
    backwards.transpose
  }
  // adds spaces to a message if it doesn't properly fit
  // in the matrix
  def padMessage(message: String): String = {
    if (message.length % matKey.byRow.length == 0) message
    else padMessage(message + " ")
  }

  // *************************************************************************************************
  // i haven't made a proper user interface, i just run the following methods from the repl
  //**************************************************************************************************

  // takes a message and a Key, produces a coded message
  def process(message: String, key: Key) =
    (for (thing <- message.toLowerCase.toList) yield numberToLetter(encode(letterToNumber(thing), key))).mkString

  // takes a string and a matrix key
  // can be used for both encoding and decoding
  def matProcess(message: String, key: Matrix) = {
    (for (thing <- matModMult(key, createMatMessage(message)).byRow.flatten) yield numberToLetter(thing)).mkString
  }

  // takes a code and a Key, recovers the message 
  def recover(message: String, key: Key) =
    (for (thing <- message.toLowerCase.toList) yield numberToLetter(decode(letterToNumber(thing), key))).mkString

  // takes two of message/code character sets, returns a Key object.
  // the pairs must be in the order message-1, code-1, message-2, code-2.
  def findKey(m1: Char, c1: Char, m2: Char, c2: Char): Key = {
    val codes = Math.abs(letterToNumber(c1) - letterToNumber(c2))
    val messages = Math.abs(letterToNumber(m1) - letterToNumber(m2))
    val inverse = BigInt(messages).modInverse(29).toInt
    val a = (codes * inverse) % 29
    val b = (posMod(letterToNumber(c1) - (a * letterToNumber(m1)))) % 29
    Key(a.toInt, b.toInt)
  }

  def createMatRecoveryKey(mat: Matrix): Matrix = Matrix.createMatRecoveryKey(mat)

  // findKey can also be used with a four character string, if you don't love
  // typing out single quotes.
  def findKey(pairs: String): Key = {
    if (pairs.length != 4)
      throw new Exception("bad number of message/code characters")
    else
      findKey(pairs(0), pairs(1), pairs(2), pairs(3))
  }

  // takes a message and two message/code character pairs, returns a message recovery attempt.
  def keyAndRecover(m: String, pairs: String) = {
    recover(m, findKey(pairs))
  }

  // prints all possible decodings that include the filter.
  def brutalize(filter: String) = for (i <- 0 to 28; j <- 0 to 28) {
    try {
      val result = keyAndRecover(s, "a" + numberToLetter(i) + "b" + numberToLetter(j)) + "\n"
      if (result contains filter)
        print("\na" + numberToLetter(i) + "b" + numberToLetter(j) + "\n" + result)
      // if the transformation isn't invertible an exception automatically gets thrown.
      // i don't have any information about the exception printed, because this
      // method is pretty much guaranteed to throw a bunch of exceptions.
    } catch { case e: Exception => {} }
  }
}
