

class Matrix(val byRow: List[List[Int]]) {

  for (thing <- byRow if thing.length != byRow(0).length)
    throw new Exception("bad matrix")

  val byCol: List[List[Int]] = this.rowToCol

  def rowToCol = (for (i <- 0 to byRow(0).length - 1) yield (for (thing <- byRow) yield thing(i))).toList

  def transpose = new Matrix(rowToCol)

  override def toString: String = (for (thing <- byRow) yield thing.mkString(" ")).mkString("\n")
}

//********************************************************************
object Matrix {

  // creates a matrix recovery key from a matrix key
  def createMatRecoveryKey(mat: Matrix): Matrix = {
    val pDown = new Matrix(modEchelonApply(augByIdent(mat)).byRow)
    var r = for (thing <- pDown.byRow) yield reversePivotDirection(thing)
    val midPoint = new Matrix(r.reverse)
    val pUp = new Matrix(modEchelonApply(midPoint).byRow)
    r = for (thing <- pUp.byRow) yield reversePivotDirection(thing)
    val finalAug = rowModularInversion(r.reverse)
    new Matrix(deAugment(finalAug))
  }

  // the last step with an augmented matrix- at this point the 
  // key matrix should only be diagonal values. the rows are multiplied
  // by the modular inverse of the diagonal value.
  def rowModularInversion(values: List[List[Int]]): List[List[Int]] = {
    (for (i <- 0 until values.length)
      yield for (thing <- values(i))
      yield (thing * (BigInt(values(i)(i)).modInverse(29)).toInt) % 29).toList
  }

  // retrieves the inverse from the augmented matrix
  def deAugment(values: List[List[Int]]): List[List[Int]] = {
    for (thing <- values) yield (thing.grouped(thing.length / 2).toList)(1)
  }

  // applies the pivoting up or pivoting down
  def modEchelonApply(mat: Matrix): Matrix = {
    var ech = mat.byRow
    var output: List[List[Int]] = Nil
    for (i <- 0 until mat.byRow.length) {
      output = output :+ ech.head
      ech = modPivot(ech, i).tail
    }
    new Matrix(output)
  }

  // pivots down or up a column modularly
  def modPivot(values: List[List[Int]], index: Int): List[List[Int]] = {
    values match {
      case x :: Nil => values
      case _ =>
        values.head +: modPivot(for (thing <- values.tail) yield modEchelonRow(values.head, thing, index, 29), index)
    }
  }

  // augments the matrix with the appropriate identity matrix
  def augByIdent(mat: Matrix) = {
    new Matrix(
      (for (i <- 0 until mat.byRow.length) yield mat.byRow(i) ::: constructIdentityRow(mat.byRow.length, i)).toList)
  }

  // creates rows for the identity matrix 
  def constructIdentityRow(length: Int, row: Int): List[Int] = {
    var list: List[Int] = Nil
    while (list.length != length)
      if (list.length == row) list = list :+ 1 else list = list :+ 0
    list
  }

  // pivoting up is accomplished by separating the matrix from the 
  // augmenting portion, reversing the orders of both, then putting
  // them back together.  the row order is reversed in a different method
  def reversePivotDirection(values: List[Int]): List[Int] = {
    (for (thing <- values.grouped(values.length / 2).toList) yield thing.reverse).flatten
  }

  // conducts modular row operations.  the pivoting row values are added to
  // the values beneath it until they are a multiple of 29, and is modded
  // to zero
  def modEchelonRow(a: List[Int], b: List[Int], index: Int, modBy: Int): List[Int] = {
    if (a.length != b.length)
      throw new Exception("bad list lengths")
    else {
      val coeff = findModderValue(a(index), b(index), modBy)
      var moddedRow = for (thing <- a) yield thing * coeff
      for (a <- (moddedRow.zipAll(b, 0, 0).map { case (x, y) => x + y })) yield a % modBy
    }
  }

  // determines what values a row must be multiplied by in order
  // to zero out the pivot column values below it
  def findModderValue(a: Int, b: Int, modBy: Int): Int = {
    var counter = 0
    var incremented = b
    while (incremented % modBy != 0) {
      counter += 1
      incremented += a
    }
    counter
  }

}
